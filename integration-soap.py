from zeep import Client
from flask import Flask
from flask import jsonify
from flask import request
from flask_cors import CORS
import logging

app = Flask(__name__)
CORS(app, support_credentials=True)


# ------------------------------------------------------------------------- # 
# ---- Metodo de logueo ---------------------------------------- # /home/app/
# ------------------------------------------------------------------------- # 
logging.basicConfig(
      format='%(asctime)s %(levelname)s %(message)s',
      datefmt='%m/%d/%Y %I:%M:%S %p',
      filename='soap.log',
      level=logging.INFO)

def sendMessage(url_wsdl, request_data):
    client = Client(url_wsdl)
    response = client.service.getCountry(**request_data)
    return response

@app.route('/soap', methods = ['GET'])
def soap():
      
    country = request.args.get('country') 
    logging.info("Parametros SOAP: {0}".format(country))
   
    wsdl_tomcat = 'http://localhost:9090/ws/countries.wsdl'
    request_data = { 'name': str(country) }
    result = sendMessage(wsdl_tomcat, request_data)
    print("Resultado peticion SOAP: {0}".format(result))
    
    logging.info("Resultado peticion SOAP: {0}".format(result))
    
    response = {}
    response['name'] = str(result['name'])
    response['population'] = str(result['population'])
    response['capital'] = str(result['capital'])
    response['currency'] = str(result['currency'])
    
    return jsonify(response)
    

# ------------------------------------------------------------------------- # 
# ---- Interfaz ----------------------------------------------------------- #
# ------------------------------------------------------------------------- # 
app.run(host='0.0.0.0', debug = True, port = 9091)